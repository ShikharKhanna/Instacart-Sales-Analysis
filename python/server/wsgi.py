import sys  # NOQA
import os
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
from python.server.search_server import application

if __name__ == "__main__":
    application.run()

import sys
import os
from python.scripts import python_postgress
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

sns.set()
color = sns.color_palette()

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
orders = python_postgress.generate_dataframe_from_table("orders")


def plot_count_of_orders():
    plt.close()
    cnt_srs = orders.groupby("user_id")["order_number"].aggregate(np.max).reset_index()
    cnt_srs = cnt_srs.order_number.value_counts()
    plt.figure(figsize=(12, 8))
    sns.barplot(cnt_srs.index, cnt_srs.values, alpha=0.8, color=color[2])
    plt.ylabel('Number of Occurrences', fontsize=12)
    plt.xlabel('Maximum order number', fontsize=12)
    plt.xticks(rotation='vertical')
    plt.tight_layout()
    return plt


def plot_frequency_of_order():
    plt.close()
    plt.figure(figsize=(12, 8))
    sns.countplot(x="order_hour_of_day", data=orders, color=color[1])
    plt.ylabel('Count', fontsize=12)
    plt.xlabel('Hour of day', fontsize=12)
    plt.xticks(rotation='vertical')
    plt.title("Frequency of order by hour of day", fontsize=15)
    plt.tight_layout()
    return plt


def plot_heat_map_of_frequency():
    plt.close()
    grouped_df = orders.groupby(["order_dow", "order_hour_of_day"])["order_number"].aggregate("count").reset_index()
    grouped_df = grouped_df.pivot('order_dow', 'order_hour_of_day', 'order_number')
    plt.figure(figsize=(12, 8))
    sns.heatmap(grouped_df)
    plt.title("Frequency of Day of week Vs Hour of day")
    plt.tight_layout()
    return plt


def plot_days_since_prior_order():
    plt.close()
    plt.figure(figsize=(12, 8))
    sns.countplot(x="days_since_prior_order", data=orders, color=color[3])
    plt.ylabel('Count', fontsize=12)
    plt.xlabel('Days since prior order', fontsize=12)
    plt.xticks(rotation='vertical')
    plt.title("Frequency distribution by days since prior order", fontsize=15)
    plt.tight_layout()
    return plt
import psycopg2
import pprint
import sys
import simplejson as json
import os
from psycopg2.extras import RealDictCursor
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))

from python.dbconfig import DbConfig
from sqlalchemy import create_engine
import pandas as pd
import numpy as np
import seaborn as sns
from datetime import date, datetime
import matplotlib.pyplot as plt

sns.set()

color = sns.color_palette()


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))

def get_connection():
    conn_string = "host='localhost' dbname='postgres' user="+DbConfig.POSTGRES_USERNAME+ " password="+DbConfig.POSTGRES_PASSWORD
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=RealDictCursor)
    return cursor, conn

def execute_postgres_select_query(table_name,num_of_records):
    cursor, conn= get_connection()
    cursor.execute("SELECT * FROM "+table_name+" LIMIT "+ str(num_of_records))
    print("Connected!\n")
    result=json.dumps(cursor.fetchall(), indent=2, use_decimal=True,default=json_serial)
    conn.close()
    return result

def run_user_query(query):
    cursor, conn = get_connection()
    cursor.execute(query)
    if query.upper().startswith('DELETE') or query.upper().startswith('UPDATE') or query.upper().startswith('MODIFY') or query.upper().startswith('DROP') or query.upper().startswith('CREATE'):
        conn.commit()
        conn.close()
        return None
    else:
        result= (json.dumps(cursor.fetchall(), indent=2, use_decimal=True))
        conn.commit()
        conn.close()
        return result

def generate_dataframe_from_table(tableName):
    engine = create_engine('postgresql://postgres:infy@123@localhost:5432/postgres')
    query="select * from "+tableName+" LIMIT 100000"
    df = pd.read_sql_query(query, con=engine)
    return df

def dump_dataframe_to_table(dataframe):
    engine = create_engine('postgresql://postgres:infy@123@localhost:5432/postgres')
    cursor, conn = get_connection()
    cursor.execute("DROP TABLE IF EXISTS recommendations")
    conn.commit()
    conn.close()
    dataframe.to_sql("recommendations", engine)

def check_if_recommendation_exists():
    cursor, conn = get_connection()
    cursor.execute("select * from information_schema.tables where table_name=%s", ('recommendations',))
    return bool(cursor.rowcount)

def get_recommendation_users():
    cursor, conn = get_connection()
    cursor.execute("select a.user_id as User_Id, string_agg(product_name, ', ' ORDER BY product_name) as Products_purchased from orders a , order_product b , top1000producttop100user c  where a.order_id=b.order_id and a.user_id=CAST(c.user_id AS integer) group by a.user_id;")
    result = json.dumps(cursor.fetchall(), indent=2, use_decimal=True, default=json_serial)
    conn.close()
    return result

def get_product_mapping(recomm_list):
    cursor, conn = get_connection()
    recomm_string = ','.join("'{0}'".format(x) for x in recomm_list)
    query="select product_id, product_name from product_col_map where column_cd in ("+ recomm_string+");"
    cursor.execute(query)
    result = json.dumps(cursor.fetchall(), indent=2, use_decimal=True, default=json_serial)
    conn.close()
    return result

def execute_insert_trigger(department):
    cursor, conn = get_connection()
    cursor.execute("select max(department_id) from department;")
    newdept = cursor.fetchone()['max']
    cursor.execute("INSERT INTO department VALUES ("+str(newdept+1)+", '"+str(department)+"');")
    conn.commit()
    conn.close()

def execute_delete_sp(department):
    cursor, conn = get_connection()
    cursor.execute("select delete_department('"+str(department)+"');")
    conn.commit()
    conn.close()

def execute_upadte_sp(currentdept , newdept):
    cursor, conn = get_connection()
    cursor.execute("select update_department('"+str(currentdept)+"','"+str(newdept)+"');")
    conn.commit()
    conn.close()

def view_backup_table(table_name,num_of_records):
    cursor, conn= get_connection()
    cursor.execute("SELECT * FROM "+table_name+" ORDER by date DESC")
    print("Connected!\n")
    result=json.dumps(cursor.fetchall(), indent=2, use_decimal=True,default=json_serial)
    conn.close()
    return result

import pandas as pd
import numpy as np
from scipy.sparse.linalg import svds
import os
from python.scripts import python_postgress

import sys

from os import path
import argparse
import time


sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))

here = path.abspath(path.dirname(__file__))


def do_predictions(data):
    data_matrix = data.as_matrix()
    user_ratings_mean = np.mean(data_matrix, axis=1)
    data_normalised = data_matrix - user_ratings_mean.reshape(-1, 1)

    U, sigma, Vt = svds(data_normalised, k=80)
    sigma = np.diag(sigma)
    all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
    predicted_df = pd.DataFrame(all_user_predicted_ratings, columns=data.columns, index=data.index)
    return predicted_df


def get_user_rating(userid, topk):
    input_data = python_postgress.generate_dataframe_from_table("recommendations")
    input_data=input_data.set_index('index')
    if int(userid) in input_data.index:
        user_data = (input_data.loc[userid][:topk])
        return (list(user_data.index))
    else:
        print("You have entered an invalid userid")
        return None


def is_user_valid(userid):
    if python_postgress.check_if_recommendation_exists():
        input = python_postgress.generate_dataframe_from_table("recommendations")
        input=input.set_index('index')
        if int(userid) in input.index:
            return True
        else:
            return False
    return True

def generate_ratings(uid, topk=5, refresh="no"):
    full_predictions=pd.DataFrame()
    top1000producttop100user = python_postgress.generate_dataframe_from_table("top1000producttop100user")
    top1000producttop100user=top1000producttop100user.astype(int)
    top1000producttop100user =top1000producttop100user.set_index('user_id')
    uid = int(uid)
    topk = int(topk)
    if refresh == "no" and python_postgress.check_if_recommendation_exists():
        return get_user_rating(uid, topk)

    else:
        if not is_user_valid(uid):
            return
        else:
            data = top1000producttop100user.copy()
            predicted_df = do_predictions(top1000producttop100user)
            for userid in data.index:
                user_row = data.loc[userid]
                non_predicted_items = list(data.columns[(user_row == 0)])
                user_prediction = predicted_df.loc[userid]
                for item in non_predicted_items:
                    user_prediction.filter(like=item)
                top_predictions = pd.DataFrame(user_prediction.sort_values(ascending=False)).T
                top_predictions.name=userid
                full_predictions=full_predictions.append(top_predictions)
            python_postgress.dump_dataframe_to_table(full_predictions)
            return get_user_rating(uid, topk)